const std = @import("std");

pub fn eql(comptime T: type, a: []const T, b: []const T) bool {
    if (a.len != b.len) return false;
    for (a, 0..) |item, index| {
        if (b[index] != item) return false;
    }
    return true;
}

pub fn main() void {
    var ok: bool = undefined;

    ok = eql(u8, "abc", "def");
    std.debug.print("string: {}\n", .{ok});

    const list: []const bool = &.{ true, false };
    ok = eql(bool, list, list);
    std.debug.print("booleans: {}\n", .{ok});
}
