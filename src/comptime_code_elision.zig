const std = @import("std");

const Op = enum { Sum, Mul, Sub };

fn apply(comptime ops: []const Op, num: i64) i64 {
    var acc: i64 = 0;
    inline for (ops) |op| {
        switch (op) {
            .Sum => acc += num,
            .Sub => acc -= num,
            .Mul => acc *= num,
        }
    }

    return acc;
}

pub fn main() void {
    const ops = [4]Op{ .Sum, .Mul, .Sub, .Sum };
    const v = 2;
    const res = apply(ops[0..], v);

    std.debug.print("Result: {}\n", .{res});
}
