const std = @import("std");

pub fn name(t: anytype) ?[]const u8 {
    const T = @TypeOf(t);
    if (@typeInfo(T) != .Struct) return null;

    if (@hasField(T, "name")) {
        return @field(t, "name");
    }

    return null;
}

pub fn main() void {
    const A = struct { name: []const u8 };

    std.debug.print("name: {s}\n", .{
        name(A{ .name = "foo" }) orelse "no name",
    });

    std.debug.print("name: {s}\n", .{
        name(12) orelse "no name",
    });
}
