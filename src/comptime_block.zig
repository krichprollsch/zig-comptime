const std = @import("std");

// check if the v string is equals to the ref.
// ref must be comptime known and uppercase.
fn eql(comptime ref: []const u8, v: []const u8) bool {
    comptime {
        for (ref) |c| {
            if (!std.ascii.isUpper(c)) {
                @compileError("`ref` must be all uppercase");
            }
        }
    }

    return std.mem.eql(u8, ref, v);
}

pub fn main() void {
    _ = eql("foo", "bar"); // program fails to compile.
}
