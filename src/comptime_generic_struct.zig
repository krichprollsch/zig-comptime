const std = @import("std");

pub fn List(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const Node = struct {
            prev: ?*Node = null,
            next: ?*Node = null,
            data: T,
        };

        first: ?*Node = null,

        pub fn prepend(list: *Self, n: *Node) void {
            n.next = list.first;
            list.first = n;
        }
    };
}

const Point = struct {
    x: u32 = 0,
    y: u32 = 0,
};
const PointList = List(Point);

pub fn main() void {
    var my_list = PointList{};
    var n = PointList.Node{
        .data = Point{ .x = 1, .y = 1 },
    };
    my_list.prepend(&n);
}
