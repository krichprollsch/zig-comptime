<!DOCTYPE html>
<html>
  <head>
    <title>Zig Comptime</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      .blockquote {

      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# Zig Comptime

---
class: center, middle

<img alt="ziglang.org homepage" src="img/zig-homepage.png" style="width:600px"/><br>
[ziglang.org](https://ziglang.org/)

---

# Zig

Small, simple language

Created by Andrew Kelley in 2016, the current version is 0.12.

* Compiled
* Statically typed
* No hidden control flow
* No hidden memory allocation
  * No garbage collector
  * Any functions that need to allocate memory accept an allocator parameter
  * `defer` and `errdefer`


[std.fmt.allocPrint](https://ziglang.org/documentation/master/std/#std.fmt.allocPrint)
```zig
pub fn allocPrint(allocator: mem.Allocator, comptime fmt: []const u8, args: anytype) AllocPrintError![]u8
```

```zig
const name = try std.fmt.allocPrint(allocator, "Device(id={d})", id);
defer allocator.free(name);
```

---

# Zig

* Easy to integrate with C
* Easy to cross compile
* [Zig build system](https://ziglang.org/learn/build-system/)
  * Replace make, cmake, ...
  * Build any C or C++ project with only Zig installed

---

# Zig Comptime

Blocks of code executed at compilation time.

* Written in pure Zig
* No support of reflection at runtime

use `comptime` keyword to force code to run at compilation time.
---

# Compile time function calls

```zig
const std = @import("std");

fn multiply(a: i64, b: i64) i64 {
    return a * b;
}

pub fn main() void {
    const len = comptime multiply(4, 5);
    const my_static_array: [len]u8 = undefined;

    std.debug.print("array length: {d}\n", .{my_static_array.len});
}
```

---

## Compile time function calls

```zig
const std = @import("std");

fn multiply(a: i64, b: i64) i64 {
    return a * b;
}

pub fn main() void {
    const len = comptime multiply(4, 5);
    const my_static_array: [len]u8 = undefined;

    std.debug.print("array length: {d}\n", .{my_static_array.len});
}
```

```zig
pub fn main() void {
    const my_static_array: [20]u8 = undefined;

    std.debug.print("array length: {d}\n", .{my_static_array.len});
}
```

---

# Compile time blocks

```zig
const std = @import("std");

// check if the v string is equals to the ref.
// ref must be comptime known and uppercase.
fn eql(comptime ref: []const u8, v: []const u8) bool {
    comptime {
        for (ref) |c| {
            if (!std.ascii.isUpper(c)) {
                @compileError("`ref` must be all uppercase");
            }
        }
    }

    return std.mem.eql(u8, ref, v);
}

pub fn main() void {
    _ = eql("foo", "bar"); // program fails to compile.
}
```

---

## Compile time blocks

Useful for checking dev code.

```console
$ zig run comptime_block.zig
comptime_block.zig:9:17: error: `ref` must be all uppercase
                @compileError("`ref` must be all uppercase");
                ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
referenced by:
    main: comptime_block.zig:18:12
    callMain: /usr/local/zig-0.12.0-dev.1773+8a8fd47d2/lib/std/start.zig:575:17
    remaining reference traces hidden; use '-freference-trace' to see all reference traces
```

---

# Compile-time code elision

Resolve control flow expressions that depend on compile-time known values.

```zig
const std = @import("std");

const Op = enum { Sum, Mul, Sub };

fn apply(comptime ops: []const Op, num: i64) i64 {
    var acc: i64 = 0;
    inline for (ops) |op| {
        switch (op) {
            .Sum => acc += num,
            .Sub => acc -= num,
            .Mul => acc *= num,
        }
    }

    return acc;
}

pub fn main() void {
    const ops = [4]Op{ .Sum, .Mul, .Sub, .Sum };
    const v = 2;
    const res = apply(ops[0..], v);

    std.debug.print("Result: {}\n", .{res});
}
```

---

## Compile-time code elision

```zig
fn apply(comptime ops: []const Op, num: i64) i64 {
    var acc: i64 = 0;
    inline for (operations) |op| {
        switch (op) {
            .Sum => acc += num,
            .Sub => acc -= num,
            .Mul => acc *= num,
        }
    }

    return acc;
}
```

```zig
// With ops = [4]Op{ .Sum, .Mul, .Sub, .Sum };
fn apply(num: i64) i64 {
    var acc: i64 = 0;
    acc += num;
    acc *= num;
    acc -= num;
    acc += num;

    return acc;
}

```

You can use `inline` with `switch`, `for`, `while` and `fn`.
But use it [carefully](https://ziglang.org/documentation/master/#inline-fn).

---
# Generic function

```zig
const std = @import("std");

pub fn eql(comptime T: type, a: []const T, b: []const T) bool {
    if (a.len != b.len) return false;
    for (a, 0..) |item, index| {
        if (b[index] != item) return false;
    }
    return true;
}

pub fn main() void {
    var ok: bool = undefined;

    ok = eql(u8, "abc", "def");
    std.debug.print("string: {}\n", .{ok});

    const list: []const bool = &.{ true, false };
    ok = eql(bool, list, list);
    std.debug.print("booleans: {}\n", .{ok});
}
```

---
## Generic function

```zig
const std = @import("std");

pub fn eql(comptime T: type, a: []const T, b: []const T) bool {
    if (a.len != b.len) return false;
    for (a, 0..) |item, index| {
        if (b[index] != item) return false;
    }
    return true;
}

pub fn main() void {
    var ok: bool = undefined;

    ok = eql(u8, "abc", "def");
    std.debug.print("string: {}\n", .{ok});

    const list: []const bool = &.{ true, false };
    ok = eql(bool, list, list);
    std.debug.print("booleans: {}\n", .{ok});
}
```

```console
$ zig run comptime_generics_fn.zig
string: false
booleans: true
```

---

# Type introspection

```zig
const std = @import("std");

pub fn name(t: anytype) ?[]const u8 {
    const T = @TypeOf(t);
    if (@typeInfo(T) != .Struct) return null;

    if (@hasField(T, "name")) {
        return @field(t, "name");
    }

    return null;
}

pub fn main() void {
    const A = struct { name: []const u8 };

    std.debug.print("name: {s}\n", .{
        name(A{ .name = "foo" }) orelse "no name",
    });

    std.debug.print("name: {s}\n", .{
        name(12) orelse "no name",
    });
}
```

The type of the
[anytype](https://ziggit.dev/t/generic-programming-and-anytype/3228) parameter
is deduced at comptime.

---

# Generic struct

```zig
const std = @import("std");

pub fn List(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const Node = struct {
            prev: ?*Node = null,
            next: ?*Node = null,
            data: T,
        };

        first: ?*Node = null,

        pub fn prepend(list: *Self, n: *Node) void {
            n.next = list.first;
            list.first = n;
        }
    };
}
```

---

## Generic struct
```zig
const Point = struct {
    x: u32 = 0,
    y: u32 = 0,
};
const PointList = List(Point);

pub fn main() void {
    var my_list = PointList{};
    var n = PointList.Node{
        .data = Point{ .x = 1, .y = 1 },
    };
    my_list.prepend(&n);
}
```

---

# References

* https://ziglang.org/documentation/0.12.0/#comptime
* https://kristoff.it/blog/what-is-zig-comptime/
* https://zig.guide/language-basics/comptime/
* https://ziggit.dev/t/generic-programming-and-anytype/3228

---

class: bottom, left


# Thank you 🙏

Pierre Tachoire<br>
pierre@tch.re<br>
[mamot.fr/@krichprollsch](https://mamot.fr/@krichprollsch)

    </textarea>
    <script src="remark-latest.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
      var slideshow = remark.create();
    </script>
  </body>
</html>
