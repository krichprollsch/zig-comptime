# Zig Comptime

Talk given during the Clermont'ech [APIHour#60](https://www.clermontech.org/api-hours/api-hour-60.html).

https://krichprollsch.gitlab.io/zig-comptime

Thanks to [remark](https://remarkjs.com/).
